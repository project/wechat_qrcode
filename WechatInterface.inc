<?php

/**
 * Class WechatInterface
 *
 * @author Xiukun zhou
 * @blog http://www.xiukun.me
 */
class WechatInterface {

  /**
   * @var string $AppId
   */
  private $AppId;

  /**
   * @var string $AppSecret
   */
  private $AppSecret;

  /**
   * @var string $AccessToken
   */
  private $AccessToken;

  /**
   * @var string $Ticket
   */
  private $Ticket;

  /**
   * @var array $Errors save all error.
   */
  private $Errors;

  // Init.
  public function __construct($AppId, $AppSecret) {
    $this->AppId = $AppId;
    $this->AppSecret = $AppSecret;
  }

  /**
   * @return mixed
   */
  private function SetAccessToken() {
    $cache_id = 'wechat_access_token';

    // check cache.
    if ($cached = cache_get($cache_id)) {
      $this->AccessToken = $cached->data;
    }
    else {
      if ($this->AppId && $this->AppSecret) {
        $request_result = drupal_http_request($this->GenerateUrl('token', array(
            'grant_type' => 'client_credential',
            'appid'      => $this->AppId,
            'secret'     => $this->AppSecret,
          )
        ));

        // @TODO check drupal_http_request code.
        $request_result = json_decode($request_result->data, TRUE);

        // cache result.
        if (isset($request_result['errcode'])) {
          $this->SetError($request_result['errorcode']);
        }
        else {
          $this->AccessToken = $request_result['access_token'];
          cache_set($cache_id, $this->AccessToken, 'cache', time() + (int) $request_result['expires_in']);
        }
      }
      else {
        $this->SetErrort(t('Please set your appid and appsecret, goto @settings_page', array(
          '@settings_page' => l('settings page', 'admin/config/wechat_info'),
        )));
      }
    }
    return $this->AccessToken;
  }

  /**
   * @return mixed
   */
  public function GetAccessToken() {
    if (!$this->AccessToken) {
      $this->SetAccessToken();
    }

    return $this->AccessToken;
  }

  /**
   * @return mixed
   */
  private function SetTicket() {
    // get token.
    if ($this->GetAccessToken()) {

      // this is request body.
      // @see https://mp.weixin.qq.com/wiki/18/28fc21e7ed87bec960651f0ce873ef8a.html
      $request_body = array(
        'action_name' => 'QR_LIMIT_SCENE',
        'action_info' => array(
          'scene' => array(
            'scene_id' => 100000,
          ),
        ),
      );

      // request to wechat.
      $request_result = drupal_http_request($this->GenerateUrl('qrcode/create', array(
        'access_token' => $this->GetAccessToken(),
      )), array(
        'data'   => json_encode($request_body),
        'method' => 'POST',
      ));

      $request_result = json_decode($request_result->data, TRUE);
      // cache result or set error.
      if (isset($request_result['errcode'])) {
        $this->SetError(t($request_result['errcode'] . ' : ' . $request_result['errmsg']));
      }
      else {
        $this->Ticket = $request_result['ticket'];
      }
    }
    return $this->Ticket;
  }

  /**
   * @return mixed
   */
  public function GetTicket() {
    if (!$this->Ticket) {
      $this->SetTicket();
    }
    return $this->Ticket;
  }

  /**
   * get qrcode.
   *
   * @return bool|object
   */
  public function GetQrCode() {
    if ($ticket = $this->GetTicket()) {
      // get ticket form wechat api.
      return drupal_http_request($this->GenerateUrl('showqrcode', array(
          'ticket' => $ticket,
        )
      ));
    }
    return FALSE;
  }

  /**
   * @param $type
   * @param $args
   *
   * @return string
   */
  public function GenerateUrl($type, $args) {
    $api = 'https://api.weixin.qq.com/cgi-bin/';
    return $api . ltrim($type, '/') . '?' . http_build_query($args);
  }

  /**
   * @param $string
   */
  private function SetError($string) {
    $this->Errors[] = $string;
  }

  /**
   * @return array
   */
  public function GetError() {
    return $this->Errors;
  }
}
